package avenuecode.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Filipe Gomes
 * @since 29/03/2017
 *
 */
@Configuration
@EnableAutoConfiguration (exclude = {SecurityAutoConfiguration.class, MultipartAutoConfiguration.class})
@ComponentScan("avenuecode.project")
public class ProjectWebApplication {	
    public static void main(String[] args) throws Exception{
        SpringApplication.run(ProjectWebApplication.class, args);
    }
}