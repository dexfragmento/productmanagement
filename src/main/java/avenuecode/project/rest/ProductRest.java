package avenuecode.project.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import avenuecode.project.entity.Picture;
import avenuecode.project.entity.Product;
import avenuecode.project.service.PictureService;
import avenuecode.project.service.ProductService;

/**
 * Resource Class to work with Products request/response
 * 
 * @author Filipe Gomes
 * @since 29/03/2017
 *
 */
@Component
@Path("/product")
public class ProductRest {

	@Autowired
	private ProductService productService;

	@Autowired
	private PictureService pictureService;

	@GET
	public Response getAllProducts() {
		return Response.ok(productService.getAllProduct()).build();
	}

	@GET
	@Path("/{id}")
	public Response getProductById(@PathParam("id") final Long id) {
		return Response.ok(productService.getProductById(id)).build();
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response saveProduct(final FormDataMultiPart formParams) throws IOException {

		final List<Picture> images = new ArrayList<Picture>();
		List<Product> products = new ArrayList<Product>();
		final Product product = new Product();
		
		Map<String, List<FormDataBodyPart>> fieldsByName = formParams.getFields();
		for (List<FormDataBodyPart> fields : fieldsByName.values()) {
			for (FormDataBodyPart field : fields) {
				
				final String splitField[] = field.getName().split("_");
				
				if(splitField[0].equals("id")) {
					String str = field.getEntityAs(String.class);
					product.setId(str == null ? null : Long.parseLong(str));
				}
				
				if (splitField[0].equals("name")) {
					String str = field.getEntityAs(String.class);
					product.setName(str);
				}
				
				if (splitField[0].equals("description")) {
					String str = field.getEntityAs(String.class);
					product.setDescription(str);
				}
				
				if (splitField[0].equals("file")) {
					Long id = (splitField.length >= 2 ? Long.parseLong(splitField[1]) : null);					
					if (id == null) {
						InputStream is = field.getEntityAs(InputStream.class);
						final Picture pic = new Picture();
						pic.setId(id);
						pic.setAttachedPicture(IOUtils.toByteArray(is));
						images.add(pic);
					}					
				}
				
				if (splitField[0].equals("productsId")) {
					String str = field.getEntityAs(String.class);
					str = str.replace("[", "");
					str = str.replace("]","");
					products = productService.arrayProducts(str);
				}
				
			}
		}

		/**
		 * Save father product
		 */
		productService.saveProduct(product);

		/**
		 * Set ManyToOne product
		 */
		for (Picture p : images) {
			p.setProduct(product);
		}

		for (Product p : products) {
			p.setProduct(product);
		}
		product.setAttachedPictures(images);
		product.setProducts(products);

		/**
		 * Save product with List
		 */
		productService.saveProduct(product);

		return Response.ok(product).build();
	}

	@DELETE
	public Response deleteAllProducts() {
		try {
			final List<Product> products = productService.getAllProduct();
			for (Product p : products) {
				p.setProduct(null);
				productService.saveProduct(p);
			}
			for (Product p : products) {
				productService.deleteProduct(p);
			}
			return Response.ok(Boolean.TRUE).status(200).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok(Boolean.FALSE).status(500).build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteProduct(@PathParam("id") final Long id) {
		final Product p = productService.getProductById(id);
		p.setProduct(null);
		productService.saveProduct(p);
		return Response.ok(productService.deleteProduct(p)).build();
	}

	@GET
	@Path("/image")
	public Response getAllImages() {
		final List<Picture> images = (List<Picture>) pictureService.getAllImage();
		return Response.ok(images).build();
	}

	@GET
	@Path("/image/{id}")
	public Response getImageById(@PathParam("id") final Long id) {
		return Response.ok(pictureService.getImageById(id)).build();
	}
}
