package avenuecode.project.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import avenuecode.project.entity.Product;

@Repository
public class DAO extends GenericDAO<Product, Long> {

	public DAO() {
		super(Product.class);
	}

	public List<Product> getAllProduct() {
		return getAll();
	}

	public Product getProductById(final Long id) {
		return getById(id);
	}

}
