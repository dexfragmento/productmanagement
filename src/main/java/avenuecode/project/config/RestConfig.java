package avenuecode.project.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.stereotype.Component;

/**
 * Class that configure JAX-RS specification in Spring Boot
 * 
 * The register of all resources classes have to stay in specified package
 * 
 * @author Filipe Gomes
 * @since 29/03/2017
 *
 */
@Component
@ApplicationPath("/")
public class RestConfig extends ResourceConfig{

	public RestConfig() {
        registerEndpoints();
    }

    private void registerEndpoints() {
    	register(MultiPartFeature.class);
    	packages("avenuecode.project.rest");
    	property(ServletProperties.FILTER_FORWARD_ON_404, true);
    }
	
}
