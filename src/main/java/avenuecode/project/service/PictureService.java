package avenuecode.project.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import avenuecode.project.dao.PictureDAO;
import avenuecode.project.entity.Picture;

@Service
public class PictureService {

	@Autowired
	private PictureDAO dao;
		
	public List<Picture> getAllImage() {
		return (List<Picture>) dao.findAll();
	}
	
	public Picture getImageById(final Long id) {
		return dao.findOne(id);
	}
	
	@Transactional
	public Boolean deletePicture(final Picture picture) {
		try{
			dao.delete(picture);
			return Boolean.TRUE;
		} catch(Exception e) {
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}
	
	public List<Long> arrayLongPictures(final String ids) {
		Long pictureId;
		final List<Long> picturesId = new ArrayList<Long>();
		final String splitId[] = ids.split(",");
		for (int i = 0; i < splitId.length; i++) {
			if (!splitId[i].equals("")) {
				pictureId = Long.parseLong(splitId[i]);
				if (pictureId != null) {
					picturesId.add(pictureId);
				}
			}
		}
		return picturesId;
	}
		
}
