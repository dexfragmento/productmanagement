package avenuecode.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author Filipe Gomes
 * @since 29/03/2017
 *
 */
@Controller
public class MainController {
    @RequestMapping(value="/",method = RequestMethod.GET)
    public String homePage(){
        return "index.html";
    }
}
