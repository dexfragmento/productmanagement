/**
 * @author Filipe Gomes
 * @since 29/03/2017
 */
(function () {
	'use strict';
	
	angular.module('app').controller('searchProductController', ['$scope', 'service', '$location', 'editedFilterSearch', function ($scope, service, $location, editedFilterSearch) {
		
		init();
		
		/**
		 * Functions
		 */
		$scope.search = search;
		$scope.cleanFilter = cleanFilter;
		$scope.viewImages = viewImages;
		$scope.setImagePreview = setImagePreview;
		$scope.edit = edit;
		$scope.deleteProduct = deleteProduct;
		$scope.deleteAllProduct = deleteAllProduct;
		$scope.loadAssociatedProducts = loadAssociatedProducts;
		
		function init () {
			$scope.filter = {};			
			$scope.products = [];
			$scope.images = [];
			$scope.associatedProducts = {};
			$scope.associatedProducts.products = [];
			
			if (editedFilterSearch) {
				$scope.filter = editedFilterSearch;
				search();
			}
		}
		
		function setImagePreview (image) {
			$scope.imagePreview = image;
		}
		
		function viewImages (id) {
			service.getProductById(id).then(
					function (result) {
						$scope.images = result.data.attachedPictures;	
						
						if ($scope.images.length >= 1) {
							$scope.imagePreview = $scope.images[0].attachedPicture;
						}
						
					},
					function (fail) {
						console.error(fail);
					});
		}
		
		function search () {
			$scope.products = [];
			if ($scope.filter.id) {
				service.getProductById($scope.filter.id).then(
						function (result) {
							if (result.data.id) {
								$scope.products.push(result.data);
							}
							$scope.tmpFilter = angular.copy($scope.filter);
						},
						function (fail) {
							console.error(fail);
						});
			} else {
				service.getAllProducts().then(
						function (result) {
							$scope.products = result.data;
							$scope.tmpFilter = angular.copy($scope.filter);
						},
						function (fail) {
							console.error(fail);
						});
			}
		}	
		
		function loadAssociatedProducts (product) {
			$scope.associatedProducts.parentProduct = product;
			$scope.associatedProducts.products = product.products;
		}
		
		function edit (id) {
			$location.path("/registerProduct").search({id: id, editedFilterSearch: $scope.tmpFilter});;
		}
		
		function deleteProduct (id) {
			service.deleteProduct(id).then(
					function (result) {
						if (result.data) {							
							removeFromListById($scope.products, id);
							removeFromListById($scope.associatedProducts.products, id);
							$scope.filter = angular.copy($scope.tmpFilter);
							search();
						}
					},
					function (fail) {
						console.error(fail);
					});
		}
		
		function removeFromListById(list, id) {
			for (var i = 0; i < list.length; i++) {
				var obj = list[i];
				if (obj.id === id) {
					list.splice(i, 1);
				}
			}
		}
		
		function deleteAllProduct () {
			service.deleteAllProduct().then(
					function (result) {
						if (result.data) {
							$scope.products = [];
							$scope.associatedProducts.products = [];
						}
					},
					function (fail) {
						console.error(fail);
					});
		}
		
		function cleanFilter () {
			$scope.filter = {};
		}
		
	}]);
	
})();