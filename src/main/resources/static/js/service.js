/**
 * @author Filipe Gomes
 * @since 30/03/2017
 */
(function () {
	'use strict';

	angular.module('app').service('service', ['$http', '$q', function ($http, $q) {
		
		function _getAllProducts () {
			return $http({
                method: 'GET',
                url: '/product'
            });
		}
		
		function _getProductById (id) {
			return $http({
				method: 'GET',
				url: '/product/' + id
			});
		}
		
		function _saveProduct (dto) {
			return $http({
				method: 'POST',
				url: '/product',
				data: dto,
				headers: {'Content-Type': undefined}
			});
		}
		
		function _deleteProduct (id) {
			return $http({
				method: 'DELETE',
				url: '/product/' + id
			});
		}
		
		function _deleteAllProduct () {
			return $http({
				method: 'DELETE',
				url: '/product'
			});
		}
		
		function _getAllImages () {
			return $http({
                method: 'GET',
                url: '/product/image'
            });
		}
		
		function _getImageById (id) {
			return $http({
				method: 'GET',
				url: '/product/image' + id
			});
		}
		
		return {
			getAllProducts: _getAllProducts,
			getProductById: _getProductById,
			saveProduct: _saveProduct,
			deleteProduct: _deleteProduct,
			deleteAllProduct: _deleteAllProduct,
			getAllImages: _getAllImages,
			getImageById: _getImageById
		}
	}]);
})();