/**
 * @author Filipe Gomes
 * @since 30/03/2017
 */
(function () {
	'use strict';
	
	angular.module('app').config(function($routeProvider, $locationProvider){
		$locationProvider.html5Mode(true);
						    $routeProvider
						        .when('/home',{
						            templateUrl: '/views/home.html',
						            controller: 'homeController'
						        })
						        .when('/registerProduct',{
						            templateUrl: '/views/registerProduct.html',
						            controller: 'registerProductController',
						            params:{
						            	id: 'id',
						            	editedFilterSearch: 'editedFilterSearch'
						            },
						            resolve: {
						            	editId: function ($route) {
						                    return $route.current.params.id;
						                },
						                editedFilterSearch: function ($route) {
						                	return $route.current.params.editedFilterSearch;
						                }
						            }
						        })
						        .when('/searchProduct',{
						            templateUrl: '/views/searchProduct.html',
						            controller: 'searchProductController',
						            params:{
						            	editedFilterSearch: 'editedFilterSearch'
						            },
						            resolve: {
						            	editedFilterSearch: function ($route) {
						                    return $route.current.params.editedFilterSearch;
						                }
						            }
						        })
						        .when('/searchImage',{
						            templateUrl: '/views/searchImage.html',
						            controller: 'searchImageController'
						        })
						        .when('/requestJson',{
						            templateUrl: '/views/allRequestJson.html',
						            controller: 'allRequestJsonController'
						        })
						        .otherwise(
						            { redirectTo: '/home'}
						        );
						});
})();