/**
 * @author Filipe Gomes
 * @since 03/04/2017
 */
(function () {
	'use strict';
	
	angular.module('app').controller('allRequestJsonController', ['$scope', 'service', function ($scope, service) {
				
		/**
		 * Functions
		 */
		$scope.getAllProducts = getAllProducts;
		$scope.getProductById = getProductById;
		$scope.getAllImages = getAllImages;
		$scope.getImageById = getImageById;
				
		function getAllProducts () {
			service.getAllProducts().then(
					function (result) {
						$scope.outputJson = result.data;
					});
		}
		
		function getProductById (id) {
			service.getProductById(id).then(
					function (result) {
						$scope.outputJson = result.data;
					});
		}
		
		function getAllImages () {
			service.getAllImages().then(
					function (result) {
						$scope.outputJson = result.data;
					});
		}
		
		function getImageById (id) {
			service.getImageById().then(
					function (result) {
						$scope.outputJson = result.data;
					});
		}
				
	}]);
	
})();